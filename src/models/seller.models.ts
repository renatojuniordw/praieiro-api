import { Column, DataType, Model, Table } from "sequelize-typescript";

@Table
export class Seller extends Model<Seller> {

    @Column({
        type: DataType.STRING,
        allowNull: false
    })
    name: string;
    
    @Column({
        type: DataType.STRING,
        allowNull: false
    })
    fantasyName: string;
    
    @Column({
        type: DataType.STRING,
        allowNull: false
    })
    phone: string;
    
    @Column({
        type: DataType.STRING,
        allowNull: false
    })
    email: string;
}