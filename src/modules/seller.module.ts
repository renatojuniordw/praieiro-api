import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';

import { Seller } from '../models/seller.models';
import { Product } from '../models/product.models';
import { Category } from '../models/category.models';

import { SellerService } from '../services/seller.service';
import { ProductService } from '../services/product.service';
import { CategoryService } from '../services/category.service';

import { SellerController } from '../controllers/seller.controller';
import { ProductController } from './../controllers/product.controller';
import { CategoryController } from './../controllers/category.controller';


@Module({
    imports: [
        SellerModule,
        SequelizeModule,
        SequelizeModule.forFeature([
            Seller,
            Category,
            Product
        ])
    ],
    controllers: [
        SellerController,
        ProductController,
        CategoryController
    ],
    providers: [
        SellerService,
        CategoryService,
        ProductService
    ],
    exports: [
        SellerService,
        CategoryService,
        ProductService
    ]
})
export class SellerModule { }
