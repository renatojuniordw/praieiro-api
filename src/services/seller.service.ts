import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';

import { Seller } from '../models/seller.models';

@Injectable()
export class SellerService {

    constructor(
        @InjectModel(Seller)
        private sellerModel: typeof Seller
    ) { }

    async getAll(): Promise<Seller[]> {
        return this.sellerModel.findAll();
    }

    async getById(id: number): Promise<Seller> {
        return this.sellerModel.findByPk(id);
    }

    async create(seller: Seller) {
        this.sellerModel.create(seller);
    }

    async uptade(seller: Seller): Promise<[number, Seller[]]> {
        return this.sellerModel.update(seller, {
            where: { id: seller.id }
        });
    }

    async delete(id: number) {
        const currentSeller = await this.getById(id);
        currentSeller.destroy();
    }
}
