import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { Seller } from '../models/seller.models';
import { SellerService } from '../services/seller.service';

@Controller('seller')
export class SellerController {

    constructor(private sellerService: SellerService) { }

    @Get('getAll')
    async getAll(): Promise<Seller[]> {
        return this.sellerService.getAll();
    }

    @Get('getById/:id')
    async getById(@Param() params): Promise<Seller> {
        return this.sellerService.getById(params.id);
    }

    @Post('create')
    async create(@Body() body: Seller) {
        this.sellerService.create(body);
    }

    @Put('uptade')
    async uptade(@Body() body: Seller): Promise<[number, Seller[]]> {
        return this.sellerService.uptade(body);
    }

    @Delete('delete/:id')
    async delete(@Param() params) {
        return this.sellerService.delete(params.id);
    }
}
